import React, { Component } from "react";
import { connect } from "react-redux";
import { changeQuatity, deleteCart } from "../Redux/actions/actionShoeShop";
class CartShoe extends Component {
  renderCartShoe = () => {
    return this.props.cartShoe.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img src={item.image} width="50" alt="" />
          </td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={() => {
                this.props.dispatch(changeQuatity(item.id, -1));
              }}
              className="btn btn-warning"
            >
              -
            </button>
            <span className="px-2">{item.quantity}</span>
            <button
              onClick={() => {
                this.props.dispatch(changeQuatity(item.id, 1));
              }}
              className=" btn btn-success"
            >
              +
            </button>
          </td>
          <td>{(item.price * item.quantity).toLocaleString()}</td>
          <td>
            <button
              onClick={() => {
                this.props.dispatch(deleteCart(item.id));
              }}
              className="btn btn-danger"
            >
              delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Image</th>
              <th>Price</th>
              <th>Quatity</th>
              <th>Total price</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderCartShoe()}</tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    cartShoe: state.ShoeShopReducer.cartShoe,
  };
};
export default connect(mapStateToProps)(CartShoe);
