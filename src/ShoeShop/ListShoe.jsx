import React, { Component } from "react";
import { connect } from "react-redux";
import ItemsShoe from "./ItemsShoe";
class ListShoe extends Component {
  renderShoe = () => {
    return this.props.shoeArr.map((item, index) => {
      return <ItemsShoe key={index} itemShoe={item} />;
    });
  };
  render() {
    return <div className="row p-4">{this.renderShoe()}</div>;
  }
}
let mapStateToProps = (state) => {
  return {
    shoeArr: state.ShoeShopReducer.shoeArr,
  };
};
export default connect(mapStateToProps)(ListShoe);
