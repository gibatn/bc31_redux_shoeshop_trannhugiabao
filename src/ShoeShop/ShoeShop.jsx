import React, { Component } from "react";
import CartShoe from "./CartShoe";
import ListShoe from "./ListShoe";

export default class ShoeShop extends Component {
  render() {
    return (
      <div className="container">
        <CartShoe />
        <ListShoe />
      </div>
    );
  }
}
