import React, { Component } from "react";
import { connect } from "react-redux";
import { addToCartAction } from "../Redux/actions/actionShoeShop";
import { ADD } from "../Redux/types/typeShoeShop";
class ItemsShoe extends Component {
  render() {
    let { image, name, price, id } = this.props.itemShoe;

    return (
      <div className="col-3">
        <div className="card text-left">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title" style={{ minHeight: 60 }}>
              {name}
            </h4>
            <p className="card-text text-primary">{price}</p>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.itemShoe);
              }}
              className="btn btn-success"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (item) => {
      dispatch(addToCartAction(item));
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemsShoe);
