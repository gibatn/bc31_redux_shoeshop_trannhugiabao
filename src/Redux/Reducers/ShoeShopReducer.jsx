import { bindActionCreators } from "redux";
import data from "../../Data/data_ShoeShop.json";
import { ADD, CHANGE_QUATITY, DELETE_CART } from "../types/typeShoeShop";
const initialState = {
  shoeArr: data,
  cartShoe: [],
};

export const ShoeShopReducer = (
  state = initialState,
  { type, payload, ...action }
) => {
  switch (type) {
    case ADD: {
      let CloneShoeArr = [...state.cartShoe];
      let index = CloneShoeArr.findIndex((item) => item.id == payload.id);
      if (index == -1) {
        // let newItem = { ...payload, quantity: 1 };
        CloneShoeArr.push({ ...payload, quantity: 1 });
      } else {
        CloneShoeArr[index].quantity++;
      }
      return { ...state, cartShoe: CloneShoeArr };
    }
    case CHANGE_QUATITY: {
      let CloneShoeArr = [...state.cartShoe];
      let index = CloneShoeArr.findIndex((item) => item.id == payload);
      if (index !== -1) {
        CloneShoeArr[index].quantity += action.num;
      }
      if (CloneShoeArr[index].quantity <= 0) {
        CloneShoeArr.splice(index, 1);
      }
      return { ...state, cartShoe: CloneShoeArr };
    }
    case DELETE_CART: {
      let CloneShoeArr = [...state.cartShoe];

      let index = CloneShoeArr.findIndex((item) => item.id == payload);
      if (index !== -1) {
        CloneShoeArr.splice(index, 1);
      }
      return { ...state, cartShoe: CloneShoeArr };
    }
  }

  return { ...state };
};
