import { ShoeShopReducer } from "./ShoeShopReducer";
import { combineReducers } from "redux";
export const rootReducer = combineReducers({
  ShoeShopReducer,
});
