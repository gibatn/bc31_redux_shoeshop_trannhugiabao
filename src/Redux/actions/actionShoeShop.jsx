import { ADD, CHANGE_QUATITY, DELETE_CART } from "../types/typeShoeShop";

export const addToCartAction = (payload) => ({
  type: ADD,
  payload,
});

export const changeQuatity = (payload, num) => ({
  type: CHANGE_QUATITY,
  payload,
  num,
});

export const deleteCart = (payload) => ({
  type: DELETE_CART,
  payload,
});
